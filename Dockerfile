FROM ruby:2.6.6-buster

RUN apt-get update && apt-get upgrade -y && apt-get install -y dos2unix

RUN apt-get install ruby ruby-json ruby-bundler  tzdata -y  

RUN apt-get install nodejs -y 

RUN apt-get install ruby-dev -y  libffi-dev && apt-get install libxslt-dev libxml2-dev  -y

RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update -qq && apt-get install -y build-essential nodejs yarn

RUN mkdir /app
WORKDIR /app

COPY Gemfile Gemfile.lock ./

RUN gem install bundler
RUN bundle install

COPY . .
RUN dos2unix ./config/docker/startup.sh && apt-get --purge remove -y dos2unix && rm -rf /var/lib/apt/lists/*

# Expose port 3000 to the Docker host, so we can access it
# from the outside.
EXPOSE 3000

RUN chmod +x ./config/docker/startup.sh

ENTRYPOINT ["sh","./config/docker/startup.sh"]





