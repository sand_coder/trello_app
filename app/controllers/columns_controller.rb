class ColumnsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_list
  before_action :set_column, only: [ :edit, :update, :destroy]

  # GET /columns/new
  def new
    @column = @list.columns.new
  end

  # GET /columns/1/edit
  def edit
  end

  # POST /columns
  # POST /columns.json
  def create
    @column = @list.columns.new(column_params)

    respond_to do |format|
      if @column.save
        format.html { redirect_to @list, notice: 'Column was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /columns/1
  # PATCH/PUT /columns/1.json
  def update
    respond_to do |format|
      if @column.update(column_params)
        format.html { redirect_to @list, notice: 'Column was successfully updated.' }
        format.json { render :show, status: :ok, location: @column }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /columns/1
  # DELETE /columns/1.json
  def destroy
    @column.destroy
    respond_to do |format|
      format.html { redirect_to @list, notice: 'Column was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_list
      @list = current_company.lists.includes(:columns).find(params[:list_id])
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_column
      @column = @list.columns.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def column_params
      params.require(:column).permit(:list_id, :name)
    end
end
