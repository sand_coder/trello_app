class CollaboratorsController < ApplicationController
	before_action :authenticate_user!

	def create
		user = User.find_by(email: params[:email])
		if user
      user_list = UserList.create(user_id: user.id, list_id: params[:list_id])
      # UserMailer.with(user_list: user_list).invite_to_collaborate.deliver_later
      # redirect_back fallback_location: root_path, notice: "Invite sent"
      redirect_back fallback_location: root_path,
                    notice: list_collaborator_url(params[:list_id], token: user_list.token)
    else
      redirect_back fallback_location: root_path, notice: "User does not exist"
    end
		
	end

	def show
		user_list = current_user.user_lists.includes(:list).find_by(token: params[:token])
    if user_list&.update(accepted: true)
      redirect_to user_list.list, notice: "Welcome to the list"
    else
      redirect root_path, notice: 'Something went wrong'
    end		
	end

	def destroy
		user_list = current_user.user_lists.find_by(list_id: params[:list_id])
    user_list.destroy
    redirect_to root_path, notice: 'You have left the list'		
	end
end