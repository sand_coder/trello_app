class ListChannel < ApplicationCable::Channel
  def subscribed
    # stream_from "some_channel"
    list = current_user.lists.find_by(params[:list_id])
    stream_for list
  end

  def unsubscribed
    Rails.logger.info "Disconnected from server"
  end
end
