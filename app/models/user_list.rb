class UserList < ApplicationRecord
  belongs_to :user
  belongs_to :list
  has_secure_token :token
end
