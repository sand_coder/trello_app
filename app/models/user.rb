class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  belongs_to :company
  accepts_nested_attributes_for :company

  has_many :user_lists, dependent: :destroy

  def lists
    List.includes(:user_lists).where(company_id: company_id)
         .or(List.includes(:user_lists).where(user_lists: { accepted: true, user_id: id }))
  end

  # has_many :company_lists, through: :company, source: :lists
  # has_many :invited_lists, through: :user_lists, source: :list
  # def lists
  #   # (company_lists + invited_lists).uniq
  #   ids = invited_lists.where(user_lists: { accepted: true}).ids + company_lists.ids
  #   List.where(ids)
  # end

  def invited_to_list?(list)
    # company_id != list.company_id
    UserList.exists?(list_id: list.id, user_id: id)
  end
end
