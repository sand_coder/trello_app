import { Controller} from "stimulus"
import consumer from '../channels/consumer'

export default class extends Controller {
  initialize() {
    this.subscription()
  }

  disconnect() {
    this.subscription().unsubscribe()
    this.subscription().disconnected()
  }

  subscription() {
    if (this._subscription == undefined) {
      let _this = this
      this._subscription = consumer.subscriptions.create({ channel: "ListChannel", list_id: this.data.get("id") }, {
        connected() {
          // Called when the subscription is ready for use on the server
          console.log(`connected to ${_this.data.get("id")}`)
        },

        disconnected() {
          // Called when the subscription has been terminated by the server
          console.log(`disconnected from ${_this.data.get("id")}`)
        },

        received(data) {
          // Called when there's incoming data on the websocket for this channel
          console.log(`received from ${_this.data.get("id")}`)
          let type = data.type
          if (type == "create") {
            _this.createItem(data.column_id, data.item)
          } else if (type == "update") {
            _this.updateItem(data.column_id, data.column)
          } else if (type == "destroy") {
            _this.destroyItem(data.item_id)
          }

        }
      });
    }
    return this._subscription
  }

  createItem(column_id, item) {
    let column = document.getElementById(`column_${column_id}`)
    column.insertAdjacentHTML("afterBegin", item)
  }

  updateItem(column_id, items) {
    let column = document.getElementById(`column_${column_id}`)
    column.innerHTML = items
  }

  destroyItem(item_id) {
    let item = document.getElementById(`item_${item_id}`)
    item.remove()
  }

}