#!/bin/sh

# Wait for DB services
# until nc -z -v -w30 $DB_HOST $DB_PORT do
#  echo 'Waiting for MySQL...'
#  sleep 1
# done
echo "Done!"

# For development check if the gems as installed, if not # then install them.
rm -f /app/tmp/pids/server.pid
echo "Done!"

# Prepare DB (Migrate - If not? Create db & Migrate)
bundle exec rails db:migrate 2>/dev/null || bundle exec rails db:create db:migrate  
echo "Done!"

# Pre-comple app assets
bundle exec rails assets:precompile   

yarn install --check-files    

echo "Assets Pre-compiled!"

#Run the application
bundle exec rails s -p 3000   -b 0.0.0.0   


