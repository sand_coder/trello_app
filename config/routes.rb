Rails.application.routes.draw do
	resources :lists do
		resources :columns, except: [:index, :show] do
			resources :items, except: [:index, :show]
		end
		resource :collaborator, only: [:create, :show, :destroy]
	end  
  
  devise_for :users
  resources :users
  resources :companies
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: 'welcome#index'
end
